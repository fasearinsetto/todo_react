import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './App.css';
import Header from './components/header/header';
import VisibleTodoList  from './containers/visibleTodoList';
import Footer from './containers/Footer';
import Loading from './components/loading';


function App() {
  return (
    <Router>
      <div className="App shadow">
        <Header appName="todos" placeholder="What needs to be done?"/> 
        <Loading/>
        <VisibleTodoList/>
        <Route path="/:filter?" component={Footer}></Route>
      </div>
    </Router>
  );
}

export default App;
