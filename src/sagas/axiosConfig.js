import axios from 'axios';

export default axios.create({
  baseURL: 'http://10.88.220.88:62506/api',
  timeout: 15000
})