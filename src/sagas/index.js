import { all } from 'redux-saga/effects';

import updateAllWatcher, {updateTodoWatcher} from './toggleTodo';
import getTodosWatcher from './getTodo';
import pushTodoWatcher from './addTodo';
import deleteTodoWatcher, {clearTodoWatcher} from './removeTodo';

export default function* rootSaga() {
  yield all([
    getTodosWatcher(),
    pushTodoWatcher(),
    deleteTodoWatcher(),
    updateAllWatcher(),
    updateTodoWatcher(),
    clearTodoWatcher(),
  ]);
};