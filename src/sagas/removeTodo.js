import { takeLatest, put, call } from 'redux-saga/effects';
import axios from './axiosConfig';

function clearTodos() {
  return axios.request({
    method: 'delete',
    url: `/Records/`,
  })
}


function* clearComplete(action) {
  try {
    if(action.id === 0) return;
    yield call(clearTodos)
    yield put({
      type: 'GET_TODOS',
      filter: action.filter
    })
  }
  catch(e) {

  }
}

export function* clearTodoWatcher() {
  yield takeLatest('CLEAR_COMPLETED', clearComplete)
}


function deleteTodo(id) {
  return axios.request({
    method: 'delete',
    url: `/Records/${id}`,
  })
}

function* removeTodo(action) {
  try {
    if(!action.id) return;
    yield call(deleteTodo, action.id);
    yield put({
      type: 'GET_TODOS', 
      filter: action.filter
    });
  }
  catch(e) {

  }
}

export default function* deleteTodoWatcher() {
  yield takeLatest('REMOVE_TODO', removeTodo)
}