import { takeLatest, put, call } from 'redux-saga/effects';
import axios from './axiosConfig';


function toggleAll(isDone) {
  return axios.request({
    method: 'put',
    url: `/Records/`,
    data: {isDone}
  })
}

function* updateAll(action) {
  console.log(action)
  try {
    yield call(toggleAll, action.isDone)
    yield put({
      type: 'GET_TODOS',
      filter: action.filter
    })
  }
  catch(e) {

  }
}

export default function* updateAllWatcher() {
  yield takeLatest('TOGGLE_TODOS', updateAll)
}

function toggleTodo(todo) {
  const body = {
    ...todo,
    isDone: !todo.isDone
  }

  return axios.request({
    method: 'put',
    url: `/Records/${todo.id}`,
    data: body
  })
}


function* updateTodo(action) {
  try {
    if(action.id === 0) return;
    yield call(toggleTodo, action.todo)
    yield put({
      type: 'GET_TODOS',
      filter: action.filter
    })
  }
  catch(e) {

  }
}

export function* updateTodoWatcher() {
  yield takeLatest('TOGGLE_TODO', updateTodo)
}