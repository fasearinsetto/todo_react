import { takeLatest, put, call } from 'redux-saga/effects';
import axiosIns from './axiosConfig';


function pushTodo(todo) {
  return axiosIns.request({
    method: 'post',
    url: '/Records',
    data: todo
  })
}

function* addTodo(action) {
  try {
    let {data} = yield call(pushTodo, action.todo);
    yield put({
      type: 'RECEIVED', 
      todos: data
    });
  }
  catch(e) {

  }
}


export default function* pushTodoWatcher() {
  yield takeLatest('ADD_TODO', addTodo)
}