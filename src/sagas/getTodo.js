import { takeLatest, put } from 'redux-saga/effects';
import axios from './axiosConfig';

function* fetchTodo(action) {
  const params = {
    'Isdel': action.filter
  }
  try {
    const {data} = yield axios.get(`/Records`, {params});
    yield put({
      type: 'RECEIVED', 
      todos: data
    });
  }
  catch(e) {
    console.error(e);
  }
}

export default function* getTodosWatcher() {
  yield takeLatest('GET_TODOS', fetchTodo)
}