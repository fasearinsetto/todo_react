const apiReducer = (state = { filter: 'All' }, action) => {
  action.filter =  action.filter || 'All';
  switch (action.type) {
    case 'GET_TODOS':
      return { ...state, filter: action.filter, loading: true }
    case 'RECEIVED':
      return { ...state, todos: action.todos, loading: false }
    case 'ADD_TODO':
      action.filter = state.filter;
      return { ...state, loading: true }
    case 'REMOVE_TODO':
      action.filter = state.filter;
      return { ...state, loading: true }
    case 'TOGGLE_TODO':
      action.filter = state.filter;
      return { ...state, loading: true }
    case 'TOGGLE_TODOS':
      action.filter = state.filter;
      return { ...state, loading: true }
    case 'CLEAR_COMPLETED':
      action.filter = state.filter;
      return { ...state, loading: true }
    default:
      return state;
  }
}

export default apiReducer;