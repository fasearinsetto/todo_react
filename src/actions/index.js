export const getTodos  = filter => ({
  type: 'GET_TODOS',
  filter
});

export const addTodo = todo => ({
  type: 'ADD_TODO',
  todo
});

export const toggleTodo = todo => ({
  type: 'TOGGLE_TODO',
  todo
});

export const removeTodo = id => ({
  type: 'REMOVE_TODO',
  id
});

export const clearCompleted = () => ({
  type: 'CLEAR_COMPLETED'
});

export const toggleTodos = isDone => ({
  type: 'TOGGLE_TODOS',
  isDone
});

export const VisibilityFilters = {
  SHOW_ALL: 'All',
  SHOW_COMPLETED: 'True',
  SHOW_ACTIVE: 'False',
}
