import React  from 'react';
import { connect } from 'react-redux'
import { addTodo } from '../../actions'
import './header.css';

function Header({ dispatch }) { 
  let input; 
  const handleSubmit = e => {
    e.preventDefault();
    if (!input.value.trim()) {
      return
    }
    let todo = {
      note: input.value,
      isDone: false
    }
    dispatch(addTodo(todo));
    input.value = "";
  }
  return (
    <header>
      <h1>todos</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" placeholder="What needs to be done?" ref={node => (input = node)}/>
      </form>
    </header>
  )
}

export default connect()(Header);