import React from 'react';

import './checkbox.css';

function Checkbox({onCompleted, todo }) {
  const handleClick = () => {
    onCompleted(todo);
  }

  return (
    <div className="container" onClick={handleClick}>
      <input type="checkbox" checked={todo.isDone} onChange={(e) => { }}/>
      <label className="checkmark"></label>
    </div>
  )
}

export default Checkbox;
