import React from 'react';
import PropTypes from 'prop-types';
import './todoItem.css';
import Checkbox from './checkbox/checkbox';

function TodoItem({todo, onCheckboxChange, onRemoveTask}) {  
  console.log(todo)
  return (
    <div className={`todo-item` }>
      <div className="view">
        <Checkbox todo={todo} onCompleted={onCheckboxChange}/>
        <label className={todo.isDone?'completed':''}>{todo.note}</label>
        <button type="button" className="remove-btn hidden" onClick={onRemoveTask}></button>
      </div>
    </div>
  )
}

TodoItem.propTypes = {
  onClick: PropTypes.func.isRequired,
  onRemoveTask: PropTypes.func.isRequired,
  todo:  PropTypes.shape({
    id: PropTypes.number.isRequired,
    isDone: PropTypes.bool.isRequired,
    note: PropTypes.string.isRequired
  }).isRequired
}

export default TodoItem;