import React from 'react';
import PropTypes from 'prop-types'
import TodoItem from '../../containers/todoItem';

import './todoList.css';
import ToggleAll from '../../containers/toggleAll';

function TodoList({todos, toggleTask}) {
  return (
    !todos || todos.length <= 0 ? null :
    <div className="todo-list">
      { todos.length > 0 ? <ToggleAll /> : '' }
      <ul>
        {
          todos.map((todo) => (
             <li key={todo.id}>
              <TodoItem onClick={() => toggleTask(todo.id)} todo={todo}></TodoItem>
            </li>
          ))
        }
      </ul>
    </div>
  )
}

TodoList.propTypes = {
  toggleTask: PropTypes.func.isRequired,
  onRemoveTask: PropTypes.func.isRequired,
}


export default TodoList;