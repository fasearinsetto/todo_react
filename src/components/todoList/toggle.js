import React  from 'react';
import PropTypes from 'prop-types'

function ToggleAll({checked, onCompleteAll}) {
  return (
    <div onClick={() => {onCompleteAll(!checked)}}>
      <label className={`all ${checked ? 'bold' : ''}`}></label>
      <input type="checkbox" className="hidden" checked={checked} onChange={e => {}}/>
    </div>
  )
}

ToggleAll.propTypes = {
  onCompleteAll: PropTypes.func.isRequired,
}


export default ToggleAll;