import React from 'react';
import { connect } from 'react-redux'

import './index.css'

let Loading = ({ loading }) => (
loading ?
<div className="modal"></div> : null
);
const mapStateToProps = (state) => ({loading: state.loading})
Loading = connect(mapStateToProps,null)(Loading)
export default Loading;