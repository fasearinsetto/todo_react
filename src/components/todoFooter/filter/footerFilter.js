import React  from 'react';
import FilterLink from '../../../containers/filterLink'

import { VisibilityFilters } from '../../../actions';

import './footerFilter.css';

function FooterFilter() {
  return (
    <ul className="filter-select">
      <FilterLink url={VisibilityFilters.SHOW_ALL}>All</FilterLink>
      <FilterLink url={VisibilityFilters.SHOW_ACTIVE}>Active</FilterLink>
      <FilterLink url={VisibilityFilters.SHOW_COMPLETED}>Completed</FilterLink>
    </ul>
  )
}

export default FooterFilter