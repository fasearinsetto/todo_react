import React from 'react';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

function TodoLink({children, active, url, onClick}) {
  return (
  <li key={children}>
    <Link className={active ? 'active' : ''} onClick={() => onClick()} to={`/${url}`}>{children}</Link>
  </li>
  )
}

TodoLink.propTypes = {
  active: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  url: PropTypes.string.isRequired
}

export default TodoLink;