import React, {useEffect} from 'react';
import PropTypes from 'prop-types'

import FooterFilter from './filter/footerFilter';
import './todoFooter.css';

function TodoFooter({todos, onClearCompleted, getTodos}) {
  useEffect(() => {
    if(!todos) {
      getTodos();
    }
  })
  return (
    <footer>
      <div className="filter more-shadow">
        <span>{(todos && todos.filter(i => !i.isDone).length) || 0 } items left</span>
        <FooterFilter/>
        <a href="javascrip:void()" onClick={() => {onClearCompleted()}}>Clear completed</a>
      </div>
    </footer>
  )
}

TodoFooter.propTypes = {
  todos:  PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      isDone: PropTypes.bool.isRequired,
      note: PropTypes.string.isRequired
    })),
  onClearCompleted: PropTypes.func.isRequired,
}

export default TodoFooter
