import { connect } from 'react-redux'
import { getTodos } from '../actions'
import TodoLink from '../components/todoFooter/filter/link';

const mapStateToProps = (state, ownProps) => ({
  active: ownProps.url === state.filter
})

const mapDispatchToProps = (dispatch, ownProps) => ({
  onClick: () => dispatch(getTodos(ownProps.url))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoLink)