import { connect } from 'react-redux'
import { removeTodo, toggleTodo } from '../actions'
import TodoItem from '../components/todoList/todoItem/todoItem';


const mapDispatchToProps = (dispatch, ownProps) => ({
  onRemoveTask: () => dispatch(removeTodo(ownProps.todo.id)),
  onCheckboxChange: todo => dispatch(toggleTodo(todo))
})

export default connect(
  null,
  mapDispatchToProps
)(TodoItem)