import { connect } from 'react-redux'
import { toggleTodos } from '../actions'
import ToggleAll from '../components/todoList/toggle';

const mapStateToProps = state => ({
  checked: state.todos.every(i => i.isDone)
})

const mapDispatchToProps = dispatch => ({
  onCompleteAll: (isDone) => dispatch(toggleTodos(isDone)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToggleAll)