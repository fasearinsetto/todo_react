import { connect } from 'react-redux';
import { toggleTodo, removeTodo } from '../actions';
import TodoList from '../components/todoList/todoList';

const mapStateToProps = state => ({
  todos: state.todos
}) 
const mapDispatchToProps = dispatch => ({
  toggleTask: id => dispatch(toggleTodo(id)),
  onRemoveTask: id => dispatch(removeTodo(id))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TodoList)