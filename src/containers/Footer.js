
import { connect } from 'react-redux';

import { clearCompleted, getTodos } from '../actions';
import todoFooter from '../components/todoFooter/todoFooter';

const mapStateToProps = (state) => ({
  todos: state.todos
})

const mapDispatchToProps = (dispatch, ownProp) => ({
  getTodos: () => dispatch(getTodos(ownProp.match.params.filter)),
  onClearCompleted: () => dispatch(clearCompleted())
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(todoFooter)
